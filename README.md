# M11- SAD Seguretat i alta disponibilitat
# UF3- IPTABLE FIREWALL
---
---
# Teoria

```
----> Output
<---- Input
```
...

---
**Modelo global**
<img>
- se abre un paq
- se toma una decicion de enrutamineto: destinado a este ordenador(INPUT) o otro ordenador(FORWARD).
- Input:
  - procesa a las reglas OUTPUT
- Forward:
  - procesar a las reglas de afuera
---



---

# Ejemplos
#### Exemple 01: política per defecte
- edita ip-default.sh
```
```
- probamos comandos

- modificamos el fichero
**añadimos nuestra IP**
```
```

- ejecutar bash
> sudo bash ip-default.sh

- probamos comandos

- descargar git profe(iptables18)
> cd /tmp/
> git clone https://github.com/edtasixm11/iptables18.git

- copiar xinetd a nuestro xinetd
> cp iptables18/xinetd/* /etc/xinetd.d/.

- copiar mismo fichero
> cp ip-default ip-01-input.sh

- editar la copia
> vim ip-01-input.sh

- ejecutamos bash

- probamos con ```wget```

- volvemos a modificar

- probamos con wget

- volvemos a modificar

- 

---

- configurar el seguiente linea dentro de ip-01-unput.sh:
```
# Obert per tothom 4080, tancat par xarxa 2hisix, obert per el i25
iptables -A INPUT -p tcp --dport 4080 -s 10.200.243.225 -j ACCEPT
iptables -A INPUT -p tcp --dport 4080 -s 10.200.243.0/24 -j DROP
iptables -A INPUT -p tcp --dport 4080 -j ACCEPT
```
> bash ip-01-input.sh

- y probar con ```telnet```:
  - con i25, dentro con ssh.
  - con otro otro ordenador (vecino)
  - con otra maquina fuera de la red 2hisix

- copiamos de nuevo el ip-default a ip-02...

- editamos el fichero

```
# Exemples OUTPUT
# Permetre accedir al port 13 de qualsevol destí
iptables -a OUTPUT -p tcp -dport 13 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 13 -d 0.0.0.0 -j ACCEPT
```
- antes de probar, primero hay que limpiar los permisos:

> bash ip-default.sh

ahora puedes seguir probando. **recuerda limpiar en cada ejemplo**

- seguimos probando las siguientes lineas:
```
# Accedir a qualsevol 2013 excepte el del i25
iptables -A OUTPUT -p tcp --dport 2013 -d 0.0.0.0 -j ACCEPT
iptables -A OUTPUT .p tcp --dport 2013 -s 10.200.243.225 -j DROP

# Obert el 3013 a tothom, tancat a 2hisix i obert a i25
iptables -A OUTPUT -p tcp --dport 3013 -d 10.200.243.225 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 3013 -d 10.200.243.0/24 -j DROP
iptables -A OUTPUT -p tcp --dport 3013 -j ACCEPT

# No es pot permés l'acces extern als ports 80,13,7
iptable -A OUTPUT -p tcp --dport 7 -j DROP
iptable -A OUTPUT -p tcp --dport 13 -j DROP
iptable -A OUTPUT -p tcp --dport 80 -j DROP

# No es pot permés l'acces extern als ports 80,13,7
iptables -A OUTPUT -p tcp --dport 7 -j REJECT
iptables -A OUTPUT -p tcp --dport 13 -j REJECT
iptables -A OUTPUT -p tcp --dport 80 -j REJECT

# No es pot accedir cap mena d'acces als hosts i02 i i04
iptables -A OUTPUT -p tcp -d 10.200.243.202 -j DROP
iptables -A OUTPUT -p tcp -d 10.200.243.204 -j DROP

# A la xarxa 2hisix només s'hi pot accedir per ssh
iptables -A OUTPUT -p tcp --dport 22 -d 10.200.243.0/24 -j ACCEPT
iptables -A OUTPUT -d 10.200.243.0/24 -j DROP
```
**para que sea mas facil probar, empieza a escribir desde arriba**

- creamos otra copia del default, como ip-03-established

- añadimos la siguente regla

```
# Exemples de trafíc relatee, established
# Permet accés a webs externes (mal fet)
iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
```

- hacemos el ejemplo bien hecho

```
# Exemples de trafíc relatee, established
# 2) Exemple ben fet: accedim a webs externes i
#	permetem el retorn de les respostes
iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -m tcp -m state \
	--state RELATED,ESTABLICHED -j ACCEPT

# 1) Permet accés a webs externes (mal fet)
iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
```

- seguir probando

```
```

---

# Practicas

---
# Teoria
- crear nou ip (ip-04)
```sh
# Exemples icmp
# 1) regalar ping
iptables -a INPUT -p icmp -j ACCEPT
iptables -a OUTPUT -p icmp -j ACCEPT
```
> sudo iptables -l

- probar 
> ping i03
> ping profen2i

- modificar
```sh
# 2)
iptables -a OUTPUT -p icmp --icmp-type 8 -j ACCEPT
iptables -a INPUT -p icmp --icmp-type 0 -j ACCEPT
iptables -a INPUT -p icmp -j DROP
iptables -a OUTPUT -p icmp -j DROP
```

- abrir un docker
> sudo systemctl stop docker
> sudo systemctl start docker

> sudo iptables -L

> docker run --rm --name a1 -h a1 --net 2hisix -d edtasixm11/net18:nethost
> docker run --rm --name a2 -h a2 --net 2hisix -d edtasixm11/net18:nethost

> docker network list

> docker network create xarxab

> docker run --rm --name a1 -h a1 --net 2hisix -d edtasixm11/net18:nethost

> docker exec -it a1 /bin/bash
> nmap a1
> nmap a2
> nmap b1
da error, no0 lo conoce

> vim ip-04...
```
#!/bin/bash
# ASIX M11-Seguretat i alta disponibilitat
# @edt 2022
# ========================================
# Activar si el host ha de fer de router
echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles Flush: buidar les regles actuals
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Establir la política per defecte (ACCEPT o DROP)
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# Filtrar ports (personalitzar)  - - - - - - - - - - - - - - - - - - - - - - -
# ----------------------------------------------------------------------------
# Permetre totes les pròpies connexions via localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Permetre tot el trafic de la pròpia ip(192.168.1.34))
iptables -A INPUT  -s 10.200.243.210 -j ACCEPT
iptables -A OUTPUT -d 10.200.243.210 -j ACCEPT

# Aplicar altres regles per obrir i tancar ports
# ....
# Aplicar altres regles per permetre o no tipus de trafic
# ....
# ----------------------------------------------------------------------------
# Mostrar les regles generades
iptables -L -t nat
```

> nmap b1

# Teoria
> docker network create netA

> docker network create netB

> docker run --rm --name hostA1 -h hostA1 --net netA --privileged -d edtasixm11/net18:nethost

> docker run --rm --name hostA2 -h hostA2 --net netA --privileged -d edtasixm11/net18:nethost

> docker run --rm --name hostB1 -h hostB1 --net netB --privileged -d edtasixm11/net18:nethost

> docker run --rm --name hostB2 -h hostB2 --net netB --privileged -d edtasixm11/net18:nethost

> iptables -L -t nat

> ping 172.22.

ping 172.23.
da error

- editar ip-05
```
#! /bin/bash
# @edt ASIX M11-SAD Curs 2018-2019
# iptables

echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Polítiques per defecte: 
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip
iptables -A INPUT -s 10.200.243.210 -j ACCEPT
iptables -A OUTPUT -d 10.200.243.210 -j ACCEPT

# Fer NAT per les xarxes internes:
# - 172.19.0.0/24
# - 172.20.0.0/24
#iptables -t nat -A POSTROUTING -s 172.19.0.0/24 -o enp6s0 -j MASQUERADE
#iptables -t nat -A POSTROUTING -s 172.20.0.0/24 -o enp6s0 -j MASQUERADE
```
- dentro del docker 

> ping 172.23.
hay ping

- editar ip.05
```
iptables -t nat -A POSTROUTING -s 172.22.0.0/24 -o eno1 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.23.0.0/24 -o eno1 -j MASQUERADE
```

```
# Exemple forward
# A1 no pot accedir a res de B1
iptables -A FORWARD -s 172.22.0.2 -s 172.23.0.2 -j DROP
```

- en el docker
> telnet 172.23.0.2 13
OK

bash ip-05

> telnet 172.23.0.2 13
se queda pillado












































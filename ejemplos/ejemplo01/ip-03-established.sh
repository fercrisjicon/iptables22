#!/bin/bash
# ASIX M11-Seguretat i alta disponibilitat
# @edt 2022
# ========================================
# Activar si el host ha de fer de router
echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles Flush: buidar les regles actuals
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Establir la política per defecte (ACCEPT o DROP)
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# Filtrar ports (personalitzar)  - - - - - - - - - - - - - - - - - - - - - - -
# ----------------------------------------------------------------------------
# Permetre totes les pròpies connexions via localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Permetre tot el trafic de la pròpia ip(192.168.1.34))
iptables -A INPUT  -s 10.200.243.210 -j ACCEPT
iptables -A OUTPUT -d 10.200.243.210 -j ACCEPT

# Exemples de trafíc relatee, established
# 4) Idem 3 prohibir a i25 d'accedeix a la nostra web
iptables -A INPUT -p tcp --dport 80 -s 10.200.243.225 -j DROP
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 80 -m tcp -m state \
	--state RELATED,ESTABLISHED -j ACCEPT

# 3) Som un servidor web, permetem accés als clients
#	a la nostra web i els responem
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 80 -m tcp -m state \
	--state RELATED,ESTABLISHED -j ACCEPT

# 2) Exemple ben fet: accedim a webs externes i
#	permetem el retorn de les respostes
iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -m tcp -m state \
	--state RELATED,ESTABLISHED -j ACCEPT

# mode paranois:
iptables -A INPUT -p tcp --sport 80 -j DROP

# 1) Permet accés a webs externes (mal fet)
iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT

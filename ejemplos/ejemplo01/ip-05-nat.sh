#! /bin/bash
# @edt ASIX M11-SAD Curs 2018-2019
# iptables

echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Polítiques per defecte: 
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip
iptables -A INPUT -s 10.200.243.210 -j ACCEPT
iptables -A OUTPUT -d 10.200.243.213 -j ACCEPT

# Fer NAT per les xarxes internes:
# - 172.19.0.0/24
# - 172.20.0.0/24
iptables -t nat -A POSTROUTING -s 172.22.0.0/24 -o eno1 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.23.0.0/24 -o eno1 -j MASQUERADE
# ....
# Exemple forward
# A1 no pot accedir a res de B1
iptables -A FORWARD -s 172.22.0.2 -d 172.23.0.2 -j DROP

# A1 no pot navegar web a l'exterior (internet)
#iptables -A FORWARD -s 172.22.0.2 -p tcp -o eno1 --dport 80 -j DROP
#iptables -A FORWARD -i eno1 -p tcp --sport 80 -d 172.22.0.2 -j DROP

# Xarxa A nomes pot navegar web per internet cap altre acces a internet
#iptables -A FORWARD -s 172.22.0.0/24 -p udp -o eno1 --dport 53 -j ACCEPT
#iptables -A FORWARD -i eno1 -p udp -d 172.22.0.0/24 --sport 53 -j ACCEPT

#iptables -A FORWARD -s 172.22.0.0/24 -p tcp -o eno1 --dport 80 -j ACCEPT
#iptables -A FORWARD -i eno1 -p tcp -d 172.22.0.0/24 --sport 80 \
#	-m state --state ESTABLISHED,RELATED -j ACCEPT
#iptables -A FORWARD -s 172.22.0.0/24 -o eno1 -j DROP
#iptables -A FORWARD -d 172.22.0.0/24 -o eno1 -j DROP

# Obrir el port 5001 --> A1:13 DNAT=NAT DESTINATION
iptables -t nat -A PREROUTING -p tcp --dport 5001 -j DNAT \
	--to 172.22.0.2:13
iptables -A FORWARD -d 172.22.0.2 -p tcp --dport 13 -j DROP
# Assegurar-se que les regles forward permeten aquest tràfic


# ----------------------------------------------------------------------------
# Mostrar les regles generades
iptables -L -t nat

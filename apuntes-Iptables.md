#### Cristian Fernando Condolo Jimenez
#### EDT 2ASIX 2021-2022
#### M11

# **Apuntes IPTABLES**

## Reglas OUTPUT

1) Router solo puede hacer __conexiones SSH__ a la __redB__:
```
iptables -A OUTPUT -d 172.20.0.0/24 -p tcp --dport 22 [-o docker1] -j ACCEPT
iptables -A INPUT -s 172.20.0.0/24 -p tcp --sport 22 [-i docker1] \
    -m state --state RELATED, STABLISHED -j ACCEPT

iptables -A OUTPUT -p tcp --dport 22 -j DROP
iptables -A INPUT -p tcp --sport 22 -j DROP
```

2) Del exterior podemos __acceder solo a las webs__ de la __red 192.168.1.0/24__ (red publica), no podemos acceder a ninguna otra web del exterior:
```
iptables -A OUTPUT -o eno1 -p tcp -d 192.168.1.0/24 --dport 80 -j ACCEPT
iptables -A INPUT -i eno1 -p tcp -s 192.168.1.0/24 --sport 80 \
    -m state --state RELATED, STABLISHED -j ACCEPT

iptables -A OUTPUT -o eno1 -p tcp --dport 80 -j DROP
iptables -A INPUT -i eno1 -p tcp --sport 80 -j DROP
```

3) Idem 2) pero __192.168.1.88__ esta __prohibido a acceder__ a su servidor web:
```
iptables -A OUTPUT -o eno1 -d 192.168.1.88 -p tcp --dport 80n -j DROP
iptables -A INPUT -i eno1 -s 192.168.1.88 -p tcp --sport 80 -j DROP

iptables -A OUTPUT -o eno1 -p tcp -d 192.168.1.0/24 --dport 80 -j ACCEPT
iptables -A INPUT -i eno1 -p tcp -s 192.168.1.0/24 --sport 80 \
    -m state --state RELATED, STABLISHED -j ACCEPT

iptables -A OUTPUT -o eno1 -p tcp --dport 80 -j DROP
iptables -A INPUT -i eno1 -p tcp --sport 80 -j DROP
```

4) Se puede __acceder al SHH__ del __host h1__ de la red net1 del exterior, __pero no__ __al SSH de otros hosts__ de net1, __pero si__ al __SSH de otros hosts externos__:
```
iptables -A OUTPUT -o eno1 -d h1 -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -i eno1 -s h1 -p tcp --sport 22 \
    -m state --state RELATED, ESTABLISHED -j ACCEPT

iptables -A OUTPUT -o eno1 -d net1/24 -p tcp --dport 22 -j DROP
iptables -A INPUT -i eno1 -s net1/24 -p tcp --sport 22 -j DROP

iptables -A OUTPUT -o eno1 -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -i eno1 -p tcp --sport 22 \
    -m state --state RELATED, ESTABLISHED -j ACCEPT
```

5) Nuestro route no se puede comunicarse para nada con la redB
```
iptables -A OUTPUT -d 172.20.0.0/24 -o docker1 -j DROP
iptables -A INPUT -s 172.20.0.0/24 -i docker1 -j DROP
```

## Reglas INPUT
6) Solo permitimos que se conecten a nuestro servidor SSH si provienen de pc1 de la redB.
Cualquier otro host de culaquier lugar no puede acceder a nuestro servidor SSH:
```
iptables -A INPUT -s 172.20.0.1 -p tcp -i docker1 --dport 22 -j ACCEPT
iptables -A OUTPUT -d 172.20.0.1 -p tcp -o docker1 --sport 22 \
    -m state --state RELATED, ESTABLISHED

iptables -A INPUT -p tcp --dport 22 -j DROP
iptables -A OUTPUT -p tcp --sport 22 -j DROP
```

7) Del exterior esta prohibido hacer ninguna conexion al router
```
iptables -a INPUT -i eno1 -j DROP
iptables -a OUTPUT -o eno1 -j DROP
```

## Reglas FORWARDING
8) La redB solo puede acceder a A2 de la redA:
```
iptables -A FORWARD -s 172.20.0.0/24 -i docker1 -d A2 -o docker0 -j ACCEPT
iptables -A FORWARD -s A2 -i docker0 -d 172.20.0.0/24 -o docker1 -j ACCEPT

iptables -A FORWARD -s 172.20.0.0/24 -i docker1 -d 172.19.0.0/24 -o docker0 -j DROP
iptables -A FORWARD -d 172.20.0.0/24 -o docker1 -s 172.19.0.0/24 -i docker0 -j DROP
```

9) La redA no puede navegar web (08) pot internet al exterior:
```
iptables -A FORWARD -s 172.19.0.0/24 -p tcp --dport 80 -o eno1 -j DROP
iptables -A FORWARS -d 172.19.0.0/24 -p tcp --sport 80 -i eno1 -j DROP 
```

## Reglas PORT FORWARDING (PREROUTING)
10) El puerto 80 (externo) del router se redirige al DMZ web1 puerto 80:
```
iptables -t nat -A PREROUTING -p tcp --dport 80 -i eno1 -j DNAT --to web1:80

iptables -A FORWARD -i eno1 -d web1 -p tcp --dport 80 -j ACCEPT
iptables -A FORWARD -o eno1 -s web1 -p tcp --sport 80 \
    -m state --state RELATED, STABLISHED -j ACCEPT
```

11) El puerto 5501 (externo) del router lleva al SSH de A1:
```
iptables -t nat -A PREROUTING -p tcp --dport 5501 -i eno1 -j DNAT --to A1:22

iptables -A FORWARD -i eno1 -d A1 -p tcp --dport 22 -j ACCEPT
iptables -A FORWARD -o eno1 -s A1 -p tpp --sport 22 \
    -m state --state RELATED, STABLISHED -j ACCEPT
```

## Reglas ICMP
12) El router no acepta peticiones ping (y no las responde):
```
iptables -A INPUT -p icmp --icmp-type 8 -j DROP
iptables -A OUTPUT -p icmp --icmp-type 0 -j DROP
```

13) Garantizar que el router si puede hacer ping:
```
iptables -A OUTPUT -p icmp --icmp-type 8 -j ACCEPT
iptables -A INPUT -p icmp --icmp-type 0 -j ACCEPT
```
